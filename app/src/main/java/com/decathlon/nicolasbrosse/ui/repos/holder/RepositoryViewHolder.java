package com.decathlon.nicolasbrosse.ui.repos.holder;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.decathlon.nicolasbrosse.data.model.Repository;
import com.decathlon.nicolasbrosse.databinding.CellRepositoryBinding;


public class RepositoryViewHolder extends RecyclerView.ViewHolder {
    private CellRepositoryBinding binding;

    public RepositoryViewHolder(View view) {
        super(view);
        binding = DataBindingUtil.bind(view);
    }

    public void bind(Repository repository) {
        binding.setRepository(repository);
    }
}