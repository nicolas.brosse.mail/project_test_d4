package com.decathlon.nicolasbrosse.component.interceptor;

import android.text.TextUtils;

import com.decathlon.nicolasbrosse.data.manager.GitHubManager;
import com.decathlon.nicolasbrosse.data.manager.OauthManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class HeaderInterceptor implements Interceptor {

    private OauthManager mOauthManager;

    public HeaderInterceptor(OauthManager oauthManager){
        this.mOauthManager = oauthManager;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        request = request.newBuilder()
                //.addHeader("Authorization", "token " + getAccessToken())
                .addHeader("Authorization", "Basic ".concat(getTokenBase64()))
                .build();
        return chain.proceed(request);
    }

    private String getTokenBase64(){
        if(null != mOauthManager){
            String usernamePasswordBase64 = mOauthManager.getUsernamePasswordBase64().replace("\n","");
            if(!TextUtils.isEmpty(usernamePasswordBase64)){
                return usernamePasswordBase64;
            }
        }
        return "";
    }

    private String getAccessToken(){
        if(null != mOauthManager){
            String accessToken = mOauthManager.getAccessToken();
            if(!TextUtils.isEmpty(accessToken)){
                return accessToken;
            }
        }
        return "";
    }
}
