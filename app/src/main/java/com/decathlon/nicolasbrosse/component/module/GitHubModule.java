package com.decathlon.nicolasbrosse.component.module;

import android.content.Context;
import android.support.annotation.NonNull;

import com.decathlon.nicolasbrosse.BuildConfig;
import com.decathlon.nicolasbrosse.component.interceptor.HeaderInterceptor;
import com.decathlon.nicolasbrosse.data.manager.DataBaseManager;
import com.decathlon.nicolasbrosse.data.manager.GitHubManager;
import com.decathlon.nicolasbrosse.data.manager.OauthManager;
import com.decathlon.nicolasbrosse.data.manager.RepositoryDao;
import com.decathlon.nicolasbrosse.data.manager.RepositoryManager;
import com.decathlon.nicolasbrosse.data.manager.impl.DataBaseManagerImpl;
import com.decathlon.nicolasbrosse.data.manager.impl.GitHubManagerImpl;
import com.decathlon.nicolasbrosse.data.manager.impl.OauthManagerImpl;
import com.decathlon.nicolasbrosse.data.manager.impl.RepositoryDaoImpl;
import com.decathlon.nicolasbrosse.data.manager.impl.RepositoryManagerImpl;
import com.decathlon.nicolasbrosse.data.persistence.OwnerRepository;
import com.decathlon.nicolasbrosse.data.service.GitHubService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */
@Module
public class GitHubModule {

    @Singleton
    @Provides
    GitHubService provideGitHubService(HeaderInterceptor headerInterceptor) {
        return getRetrofitBuilder(headerInterceptor).create(GitHubService.class);
    }

    @Singleton
    @Provides
    OauthManager provideOauthManager(OwnerRepository ownerRepository) {
        return new OauthManagerImpl(ownerRepository);
    }

    @Singleton
    @Provides
    Gson providesGson(){
        return new GsonBuilder().create();
    }

    @Singleton
    @Provides
    OwnerRepository provideOwnerRepository(Context context, Gson gson) {
        return new OwnerRepository(context, gson);
    }

    @Singleton
    @Provides
    GitHubManager provideGitHubManager(GitHubService gitHubService, OauthManager oauthManager) {
        return new GitHubManagerImpl(gitHubService, oauthManager);
    }

    @Provides
    @Singleton
    HeaderInterceptor provideHeaderInterceptor(OauthManager oauthManager) {
        return new HeaderInterceptor(oauthManager);
    }

    @Provides
    @Singleton
    DataBaseManager provideDataBaseManager(Context context) {
        return new DataBaseManagerImpl(context);
    }

    @Provides
    @Singleton
    RepositoryDao provideRepositoryDao(DataBaseManager dataBaseManager){
        return new RepositoryDaoImpl(dataBaseManager);
    }

    @Provides
    @Singleton
    RepositoryManager provideRepositoryManager(RepositoryDao repositoryDao){
        return new RepositoryManagerImpl(repositoryDao);
    }

    @NonNull
    private Retrofit getRetrofitBuilder(HeaderInterceptor headerInterceptor) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        httpClient.addInterceptor(headerInterceptor);
        httpClient.addInterceptor(httpLoggingInterceptor);

        return new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build())
                .build();
    }
}
