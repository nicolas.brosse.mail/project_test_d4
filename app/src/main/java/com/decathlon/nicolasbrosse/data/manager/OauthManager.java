package com.decathlon.nicolasbrosse.data.manager;

import com.decathlon.nicolasbrosse.data.model.LoginAccess;
import com.decathlon.nicolasbrosse.data.model.Owner;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public interface OauthManager {

    String getAccessToken();

    void setUsernamePasswordBase64(String username, String password);

    String getUsernamePasswordBase64();

    void saveAccess(LoginAccess loginAccess);

    LoginAccess getAccess();

    void saveOwner(Owner owner);

    Owner getOwner();

    boolean isConnected();


}
