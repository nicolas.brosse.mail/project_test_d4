package com.decathlon.nicolasbrosse.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class Repository implements Parcelable, Serializable{

    private String id;

    private String name;

    @SerializedName("full_name")
    private String fullName;

    private String description;

    private String homepage;

    @SerializedName("watchers_count")
    private String watchersCount;

    private Owner owner;

    private Permission permissions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getWatchersCount() {
        return watchersCount;
    }

    public void setWatchersCount(String watchersCount) {
        this.watchersCount = watchersCount;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Permission getPermissions() {
        return permissions;
    }

    public void setPermissions(Permission permissions) {
        this.permissions = permissions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.fullName);
        dest.writeString(this.description);
        dest.writeString(this.homepage);
        dest.writeString(this.watchersCount);
        dest.writeParcelable(this.owner, flags);
        dest.writeParcelable(this.permissions, flags);
    }

    public Repository() {
    }

    protected Repository(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.fullName = in.readString();
        this.description = in.readString();
        this.homepage = in.readString();
        this.watchersCount = in.readString();
        this.owner = in.readParcelable(Owner.class.getClassLoader());
        this.permissions = in.readParcelable(Permission.class.getClassLoader());
    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel source) {
            return new Repository(source);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
}
