package com.decathlon.nicolasbrosse.data.persistence;

import android.app.Application;
import android.content.Context;

import com.decathlon.nicolasbrosse.data.model.LoginAccess;
import com.decathlon.nicolasbrosse.data.model.Owner;
import com.google.gson.Gson;

public class OwnerRepository extends PreferencesRepository {

    private static final String OWNER_PREFERENCES = "owner_preferences";
    private static final String OWNER_KEY = "owner";
    private static final String OWNER_LOGIN_ACCESS = "login_access";

    public OwnerRepository(Context application, Gson gson) {
        super(application, OWNER_PREFERENCES, gson);
    }

    public void saveOwner(Owner owner) {
        saveObject(OWNER_KEY, owner);
    }

    public Owner findOwner() {
        return findObject(OWNER_KEY, Owner.class);
    }

    public void deleteOwner() {
        delete(OWNER_KEY);
    }

    public void saveAccess(LoginAccess loginAccess) {
        saveObject(OWNER_LOGIN_ACCESS, loginAccess);
    }

    public LoginAccess findLoginAccess() {
        return findObject(OWNER_LOGIN_ACCESS, LoginAccess.class);
    }


}