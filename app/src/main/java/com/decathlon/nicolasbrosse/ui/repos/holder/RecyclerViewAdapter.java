package com.decathlon.nicolasbrosse.ui.repos.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.decathlon.nicolasbrosse.R;
import com.decathlon.nicolasbrosse.data.model.Repository;

import java.util.ArrayList;
import java.util.List;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RepositoryViewHolder> {

    private List<Repository> mRepositories = new ArrayList<>();

    private OnClickListenerCustom mOnClickListenerCustom;

    public RecyclerViewAdapter() {
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View statusContainer = LayoutInflater.from(context).inflate(R.layout.cell_repository, parent, false);
        return new RepositoryViewHolder(statusContainer);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, final int position) {
        Repository repository = mRepositories.get(position);
        holder.bind(repository);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mOnClickListenerCustom) {
                    mOnClickListenerCustom.onClick(position);
                }
            }
        });
    }

    public void addAll(List<Repository> repositories) {
        mRepositories.clear();
        mRepositories.addAll(repositories);
        notifyDataSetChanged();
    }

    public Repository getItem(int position){
        return mRepositories.get(position);
    }

    @Override
    public int getItemCount() {
        return mRepositories.size();
    }

    public void setOnClickListenerCustom(OnClickListenerCustom onClickListenerCustom){
        this.mOnClickListenerCustom = onClickListenerCustom;
    }

    public interface OnClickListenerCustom {
        void onClick(int position);
    }
}