package com.decathlon.nicolasbrosse.data.model.dto;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by BROSSE Nicolas on 13/04/2017.
 */

public class RepositoryDto extends RealmObject{

    @PrimaryKey
    private String id;

    private String name;

    private String fullName;

    private String description;

    private String homepage;

    private String watchersCount;

    private OwnerDto owner;

    private PermissionDto permissions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getWatchersCount() {
        return watchersCount;
    }

    public void setWatchersCount(String watchersCount) {
        this.watchersCount = watchersCount;
    }

    public OwnerDto getOwner() {
        return owner;
    }

    public void setOwner(OwnerDto owner) {
        this.owner = owner;
    }

    public PermissionDto getPermissions() {
        return permissions;
    }

    public void setPermissions(PermissionDto permissions) {
        this.permissions = permissions;
    }
}
