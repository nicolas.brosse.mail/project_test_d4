package com.decathlon.nicolasbrosse.data.model.dto;

import io.realm.RealmObject;

/**
 * Created by BROSSE Nicolas on 13/04/2017.
 */

public class OwnerDto extends RealmObject {

    private String id;

    private String login;

    private String avatarUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
