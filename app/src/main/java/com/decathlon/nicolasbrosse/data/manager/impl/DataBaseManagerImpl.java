package com.decathlon.nicolasbrosse.data.manager.impl;

import android.content.Context;
import android.util.Log;

import com.decathlon.nicolasbrosse.data.manager.DataBaseManager;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */
public class DataBaseManagerImpl implements DataBaseManager {

    public static final String TAG = DataBaseManagerImpl.class.getSimpleName();

    private Context mContext;

    private Realm mRealm;

    @Inject
    public DataBaseManagerImpl(Context context){
        this.mContext = context;
    }

    @Override
    public Realm getRealm() {

        if (mRealm == null) {
            // create the Realm configuration
            Realm.init(mContext);
            RealmConfiguration mRealmConfiguration = new RealmConfiguration.Builder().build();

            try {
                mRealm = Realm.getInstance(mRealmConfiguration);
            } catch (RealmMigrationNeededException e) {
                Log.w(TAG, "Impossible de recuperer l'instance de Realm", e);
                try {
                    Realm.deleteRealm(mRealmConfiguration);
                    //Realm file has been deleted.
                    mRealm = Realm.getInstance(mRealmConfiguration);
                } catch (Exception ex) {
                    //No Realm file to remove.
                    Log.w(TAG, "Impossible de recuperer l'instance de Realm", ex);
                }
            }
        }
        return mRealm;
    }
}