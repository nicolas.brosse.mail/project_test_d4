package com.decathlon.nicolasbrosse.data.manager.impl;

import com.decathlon.nicolasbrosse.data.manager.DataBaseManager;
import com.decathlon.nicolasbrosse.data.manager.RepositoryDao;
import com.decathlon.nicolasbrosse.data.model.Repository;
import com.decathlon.nicolasbrosse.data.model.dto.RepositoryDto;
import com.decathlon.nicolasbrosse.ui.utils.RepositoryUtils;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by BROSSE Nicolas on 13/04/2017.
 */

public class RepositoryDaoImpl implements RepositoryDao {

    private static final String FIELD_NAME = "name";

    private DataBaseManager mDataBaseManager;

    public RepositoryDaoImpl(DataBaseManager dataBaseManager) {
        this.mDataBaseManager = dataBaseManager;
    }

    @Override
    public void saveRepositories(final List<Repository> repositories) {

        if (null != repositories && repositories.size() == 0) {
            return;
        }

        mDataBaseManager.getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                List<RepositoryDto> repositoryDtos = RepositoryUtils.convertListRepotoListRepoDto(repositories);
                realm.copyToRealmOrUpdate(repositoryDtos);
            }
        });
    }

    @Override
    public List<Repository> getRepositories() {
        mDataBaseManager.getRealm().beginTransaction();
        RealmResults<RepositoryDto> repositoryDtos = mDataBaseManager.getRealm().where(RepositoryDto.class).findAll();
        mDataBaseManager.getRealm().commitTransaction();
        return RepositoryUtils.convertListRepoDtotoListRepo(repositoryDtos);
    }

    @Override
    public Repository getRepository(int id) {
        RepositoryDto repositoryDto = mDataBaseManager.getRealm().where(RepositoryDto.class).equalTo(FIELD_NAME, String.valueOf(id)).findFirst();
        return RepositoryUtils.convertRepositoryDtoToRepository(repositoryDto);
    }

    @Override
    public void createOrUpdate(final Repository repository) {
        if(null == repository){
            return;
        }
        mDataBaseManager.getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RepositoryDto repositoryDtos = RepositoryUtils.convertRepositoryToRepositoryDto(repository);
                realm.copyToRealmOrUpdate(repositoryDtos);
            }
        });
    }

    @Override
    public void delete(Repository repository) {
        if(null == repository){
            return;
        }
        mDataBaseManager.getRealm().beginTransaction();
        RealmObject.deleteFromRealm(mDataBaseManager.getRealm().where(RepositoryDto.class).equalTo(FIELD_NAME, repository.getName()).findFirst());
        mDataBaseManager.getRealm().commitTransaction();
    }
}
