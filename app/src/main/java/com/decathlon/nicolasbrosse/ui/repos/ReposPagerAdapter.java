package com.decathlon.nicolasbrosse.ui.repos;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.decathlon.nicolasbrosse.R;

public class ReposPagerAdapter extends FragmentStatePagerAdapter {

    private Context mContext;

    public ReposPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
    }
 
    @Override
    public Fragment getItem(int position) {

        Bundle extras = new Bundle();
        extras.putBoolean("screenListRepos", 0 == position);

        ListRepoTabFragment listRepoTabFragment = new ListRepoTabFragment();
        listRepoTabFragment.setArguments(extras);
        return listRepoTabFragment;
    }
 
    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(0 == position){
            return mContext.getString(R.string.repos_tab_my_repo);
        }else {
            return mContext.getString(R.string.repos_tab_search);
        }
    }
}