package com.decathlon.nicolasbrosse.ui.repos;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.decathlon.nicolasbrosse.GitHubApplication;
import com.decathlon.nicolasbrosse.R;
import com.decathlon.nicolasbrosse.data.manager.OauthManager;
import com.decathlon.nicolasbrosse.data.model.Owner;
import com.decathlon.nicolasbrosse.ui.base.BaseFragment;
import com.decathlon.nicolasbrosse.ui.utils.ViewUtils;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class ReposFragment extends BaseFragment {

    @Inject
    OauthManager oauthManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.repos_tab_layout)
    TabLayout mTabLayout;

    @BindView(R.id.repos_pager)
    ViewPager mViewPager;

    @BindView(R.id.repos_iv_picture)
    ImageView mIvPicture;

    @BindView(R.id.repos_tv_title)
    TextView mTvTitle;

    @Override
    public String getTitle() {
        Owner owner = oauthManager.getOwner();
        if (null != owner) {
            return owner.getLogin();
        }
        return "";
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_repos;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GitHubApplication.getAppGitHubComponent(getActivity()).inject(this);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        ReposPagerAdapter reposPagerAdapter = new ReposPagerAdapter(getFragmentManager(), getActivity());
        mViewPager.setAdapter(reposPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        Owner owner = oauthManager.getOwner();

        if(null == owner){
            return;
        }

        String login = owner.getLogin();
        String avatarUrl = owner.getAvatarUrl();

        if(!TextUtils.isEmpty(login)){
            mTvTitle.setText(login);
        }

        if(!TextUtils.isEmpty(avatarUrl)){
            Glide.with(getActivity()).load(avatarUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(mIvPicture) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mIvPicture.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }
}
