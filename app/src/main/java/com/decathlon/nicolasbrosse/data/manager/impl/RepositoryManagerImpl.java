package com.decathlon.nicolasbrosse.data.manager.impl;

import com.decathlon.nicolasbrosse.data.manager.RepositoryDao;
import com.decathlon.nicolasbrosse.data.manager.RepositoryManager;
import com.decathlon.nicolasbrosse.data.model.Repository;

import java.util.List;

/**
 * Created by BROSSE Nicolas on 13/04/2017.
 */

public class RepositoryManagerImpl implements RepositoryManager{

    private RepositoryDao mRepositoryDao;

    public RepositoryManagerImpl(RepositoryDao repositoryDao){
        this.mRepositoryDao = repositoryDao;
    }

    @Override
    public void saveRepositories(List<Repository> repositories) {
        mRepositoryDao.saveRepositories(repositories);
    }

    @Override
    public List<Repository> getRepositories() {
        return mRepositoryDao.getRepositories();
    }

    @Override
    public Repository getRepository(int id) {
        return mRepositoryDao.getRepository(id);
    }

    @Override
    public void createOrUpdate(Repository repository) {
        mRepositoryDao.createOrUpdate(repository);
    }

    @Override
    public void delete(Repository repository) {
        mRepositoryDao.delete(repository);
    }
}
