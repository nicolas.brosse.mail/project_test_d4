package com.decathlon.nicolasbrosse.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class LoginAccess implements Parcelable {

    private String mLdapLogin;
    private String mPassword;

    public LoginAccess(String ldapLogin, String password) {
        mLdapLogin = ldapLogin;
        mPassword = password;
    }

    public String getUsername() {
        return mLdapLogin;
    }

    public String getPassword() {
        return mPassword;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mLdapLogin);
        dest.writeString(this.mPassword);
    }

    public LoginAccess() {
        //Empty Constructor
    }

    protected LoginAccess(Parcel in) {
        this.mLdapLogin = in.readString();
        this.mPassword = in.readString();
    }

    public static final Creator<LoginAccess> CREATOR = new Creator<LoginAccess>() {
        @Override
        public LoginAccess createFromParcel(Parcel source) {
            return new LoginAccess(source);
        }

        @Override
        public LoginAccess[] newArray(int size) {
            return new LoginAccess[size];
        }
    };
}