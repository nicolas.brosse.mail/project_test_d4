package com.decathlon.nicolasbrosse.data.service;

import com.decathlon.nicolasbrosse.data.model.Owner;
import com.decathlon.nicolasbrosse.data.model.Repository;
import com.decathlon.nicolasbrosse.data.model.Search;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public interface GitHubService {

    @GET("user")
    Observable<Owner> getUser();

    @GET("user/repos")
    Observable<List<Repository>> getRepositories();

    @GET("search/repositories")
    Observable<Search> searchRepositories(@Query("q") String q);

    @POST("user/repos")
    Observable<Repository> createRepository(@Body Repository repository);

    @PATCH("repos/{owner}/{repositoryName}")
    Observable<Repository> updateRepository(@Path("owner") String owner, @Path("repositoryName") String repositoryName, @Body Repository repository);

    @DELETE("repos/{owner}/{repositoryName}")
    Observable<Response<Void>> deleteRepository(@Path("owner") String owner, @Path("repositoryName") String repositoryName);


}
