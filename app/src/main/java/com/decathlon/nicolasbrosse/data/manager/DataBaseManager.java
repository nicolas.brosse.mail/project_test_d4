package com.decathlon.nicolasbrosse.data.manager;

import io.realm.Realm;
/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */
public interface DataBaseManager {

    Realm getRealm();
}