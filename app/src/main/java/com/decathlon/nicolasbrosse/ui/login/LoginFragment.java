package com.decathlon.nicolasbrosse.ui.login;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.decathlon.nicolasbrosse.GitHubApplication;
import com.decathlon.nicolasbrosse.R;
import com.decathlon.nicolasbrosse.data.manager.GitHubManager;
import com.decathlon.nicolasbrosse.data.manager.OauthManager;
import com.decathlon.nicolasbrosse.data.manager.RepositoryManager;
import com.decathlon.nicolasbrosse.data.model.ModelZipLoginAndRepositories;
import com.decathlon.nicolasbrosse.data.model.Owner;
import com.decathlon.nicolasbrosse.data.model.Repository;
import com.decathlon.nicolasbrosse.ui.base.BaseFragment;
import com.decathlon.nicolasbrosse.ui.repos.ReposFragment;
import com.decathlon.nicolasbrosse.ui.utils.ViewUtils;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class LoginFragment extends BaseFragment {

    @Inject
    GitHubManager gitHubManager;

    @Inject
    OauthManager oauthManager;

    @Inject
    RepositoryManager repositoryManager;

    @BindView(R.id.login_et_username)
    EditText mEtUsername;

    @BindView(R.id.login_et_password)
    EditText mEtPassword;

    @BindView(R.id.login_bt_connection)
    Button mBtConnection;

    private CompositeDisposable compositeDisposable;

    @Override
    public String getTitle() {
        return getActivity().getString(R.string.login_title);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GitHubApplication.getAppGitHubComponent(getActivity()).inject(this);

        compositeDisposable = new CompositeDisposable();

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(getActivity().getString(R.string.login_title));

        if (oauthManager.isConnected()) {
            goReposFragment(new ReposFragment());
        }

        mEtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    ViewUtils.hideKeyboard(getActivity());
                    onClickBtConnection();
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.login_bt_connection)
    public void onClickBtConnection() {

        if(TextUtils.isEmpty(mEtUsername.getText().toString())){
            mEtUsername.setError(getString(R.string.create_repo_field_required));
            return;
        }

        if(TextUtils.isEmpty(mEtPassword.getText().toString())){
            mEtPassword.setError(getString(R.string.create_repo_field_required));
            return;
        }

        Observable<Owner> observableLogin = gitHubManager.login(mEtUsername.getText().toString(), mEtPassword.getText().toString());
        Observable<List<Repository>> observableGetRepositories = gitHubManager.getRepositories();

        Disposable disposable = Observable.zip(observableLogin, observableGetRepositories, new BiFunction<Owner, List<Repository>, ModelZipLoginAndRepositories>() {
            @Override
            public ModelZipLoginAndRepositories apply(@NonNull Owner owner, @NonNull List<Repository> repositories) throws Exception {
                return new ModelZipLoginAndRepositories(owner, repositories);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .singleOrError()
                .subscribe(new BiConsumer<ModelZipLoginAndRepositories, Throwable>() {
                    @Override
                    public void accept(@NonNull ModelZipLoginAndRepositories zipLoginAndRepositories, @NonNull Throwable throwable) throws Exception {
                        if (null != zipLoginAndRepositories && null != zipLoginAndRepositories.getOwner() && null != zipLoginAndRepositories.getRepositories()) {
                            List<Repository> repositories = zipLoginAndRepositories.getRepositories();
                            repositoryManager.saveRepositories(repositories);
                            oauthManager.saveOwner(zipLoginAndRepositories.getOwner());
                            goReposFragment(new ReposFragment());
                        }

                        if(null != throwable){
                            if(((HttpException) throwable).code() == 401){
                                Toast.makeText(getActivity(), R.string.login_wrong, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(getActivity(), R.string.create_repo_error_server, Toast.LENGTH_SHORT).show();
                            }
                        }

                        mBtConnection.setEnabled(true);
                    }
                });

        compositeDisposable.add(disposable);

        mBtConnection.setEnabled(false);
    }

    @OnClick(R.id.login_bt_create)
    public void onClickBtCreation() {

    }

    private void goReposFragment(ReposFragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.main_container, fragment);
        fragmentTransaction.commit();
    }
}
